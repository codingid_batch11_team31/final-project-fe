import React from 'react';
import { Container, Grid, Typography, Link, Stack } from '@mui/material';
import Kontak1 from '.././assets/kontak1.png';
import Kontak2 from '.././assets/kontak2.png';
import Kontak3 from '.././assets/kontak3.png';
import Kontak4 from '.././assets/kontak4.png';
import Kontak5 from '.././assets/kontak5.png';

const Footer = () => {
  return (
    
      <Container maxWidth="100%" style={{ backgroundColor: "#F2C94C", paddingBottom: '20px' }}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={4}>
            <Typography variant="h6">Tentang Kami</Typography>
            <Typography>
              Apel Musik: Tempat berkembang, menyatukan para pecinta musik. Kursus terbaik, mentor berpengalaman. Realisasikan bakatmu, raih prestasi! Apel Musik: Inspirasi musikalitas. Instruktur handal, kursus berkualitas. Temukan passionmu, wujudkan mimpi musikmu bersama kami! 
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} >
            <Typography variant="h6">Produk</Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
              <ul>
                <li>Biola</li>
                <li>Gitar</li>
                <li>Drum</li>
              </ul>
              </Grid>
              <Grid item xs={12} sm={6}>
                <ul>
                  <li>Menyanyi</li>
                  <li>Piano</li>
                  <li>Saxophone</li>
                </ul>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Grid item xs={12} sm={12}>
              <Typography variant="h6">Alamat</Typography>
              <Typography>
              1 Apple Park Way · Cupertino, California · Amerika Serikat · 37°20′5″N 122°0′32″W﻿ / ﻿37.33472°N 122.00889°W﻿ / 37.33472; -122.00889. 
              </Typography>
            </Grid>
            <br/>
            <Stack spacing={2}>
              <Typography variant="h6">Kontak</Typography>
              <Stack spacing={2} direction="row">
                <img src={Kontak1} alt="Kontak1" width="40" height="40" />
                <img src={Kontak2} alt="Kontak2" width="40" height="40" />
                <img src={Kontak3} alt="Kontak3" width="40" height="40" />
                <img src={Kontak4} alt="Kontak4" width="40" height="40" />
                <img src={Kontak5} alt="Kontak5" width="40" height="40" />
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Container>
    
  );
};

export default Footer;