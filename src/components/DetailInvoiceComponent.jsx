import React from 'react';
import { Box, Button, Container, Grid, Paper, Stack, Typography } from "@mui/material";
import { Link } from "react-router-dom";


export default function DetailInvoiceComponent (){
        return (
            <>
            <Container >
                    <Grid Container spacing ={4} sx = {{marginTop : '100px'}}>
                            <Grid item lg ={12} xs= {12}>
                                <Stack direction='row'>
                                <Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginBottom: '20px',  }}>
                                Beranda
                                <span style={{ marginLeft: '10px' }}>&gt;</span>
                                </Typography>
                                       
                                <Typography Typography variant="body2" style={{ marginLeft: '10px',display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginBottom: '20px' }}>
                                          Invoice
                                </Typography>
                                <span style={{ marginLeft: '10px' }}>&gt;</span>
                                <Typography Typography variant="body2" style={{ marginLeft: '10px',display: 'flex', alignItems: 'center', fontWeight: 'light', color: '#5D5FEF', marginBottom: '20px' }}>
                                          Detail Invoice
                                </Typography>
                                </Stack>
                            </Grid>
                    </Grid>


                    <Grid Container spacing ={6} sx = {{marginTop : '10px', marginBottom: '20px' }}>
                            <Grid item lg ={12} xs= {4}>
                            
                                <Typography Typography variant="body1" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '20px' }}>
                                         Rincian Invoice
                                </Typography>
                                <Stack spacing = {6} direction = 'row'>
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black' }}>
                                         No. Invoice :
                                </Typography>
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black' }}>
                                         APM00003
                                </Typography>
                              </Stack>
                              <Stack spacing = {4} direction = 'row'>
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '20px' }}>
                                         Tanggal Beli :
                                </Typography>
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '20px' }}>
                                         12 Juni 2022
                                </Typography>
                              </Stack>

                            </Grid>
                    </Grid>

                    
                    <Grid Container spacing ={4} sx = {{ marginBottom : '10px', display: 'flex', flexDirection: 'column'}}>
                            <Grid item lg ={12} xs= {3} sx ={{backgroundColor: '#F2C94C', padding: '8px'}}>
                            <Stack  spacing = {10} direction = 'row'>
                            
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                         No
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                         Nama Course
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                         Kategori

                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                        Jadwal
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px' ,marginTop: '10px',}}>
                                         Harga
                                </Typography>

                                
                            </Stack>
                                </Grid>
                                </Grid>

                                
                        <Grid Container spacing ={4} sx = {{ marginBottom : '10px', display: 'flex', flexDirection: 'column'}}>
                           <Grid item lg ={12} xs= {3} sx ={{backgroundColor: '#white', padding: '8px'}}>
                            <Stack  spacing = {10} direction = 'row' >
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', }}>
                                         1
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '100px'}}>
                                         APM00003
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black',marginRight: '0px',marginLeft: '80px'  }}>
                                         12 Juni 2022

                                </Typography>

                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '90px' }}>
                                         2
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '150px' }}>
                                         IDR 11.500.000
                                </Typography>
                                
                                
                            </Stack>

                      </Grid>
                {/* ===== =============================================================================================================================================================================================*/}
                        <Grid Container spacing ={4} sx = {{ marginBottom : '10px', display: 'flex', flexDirection: 'column',marginBottom : '250px'}}>
                        <Grid item lg ={12} xs= {3} sx ={{backgroundColor: '#F2C94C33', padding: '8px'}}>
                            <Stack  spacing = {10} direction = 'row' >
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', }}>
                                         2
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '100px'}}>
                                         APM00002
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black',marginRight: '0px',marginLeft: '80px'  }}>
                                         12 Juni 2022

                                </Typography>

                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '90px' }}>
                                         1
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '150px' }}>
                                         IDR 11.500.000
                                </Typography>
                                
                                 
                            </Stack>
                            
                        </Grid>
                        </Grid>
 {/* ===== =============================================================================================================================================================================================*/}
                        
                    </Grid>
                    
            </Container>
            </>
        )
}