
import React, { useEffect, useState } from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button'
import { Box, Container, Grid, Paper, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import FotoDetailKelas from '../assets/DetailKelas1.png'
import DetailKelasComboBox from './DetailKelasComboBox';




const DetailKelas1 = () => {
  return (
        <>
                        
                        
                <Container>
                <Grid container spacing = {4} style ={{marginTop : '100px'}}>
                        <Grid item xs ={12} lg ={6}>
                        
                        <Link to ='/listmenu'style={{textDecoration: 'none' }}>
                        <Paper style={{ backgroundColor:'#fff', boxShadow: 'none', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'start',marginBottom : '5px',borderRadius: 0 }}>
                        <img src= {FotoDetailKelas} alt= 'Drum' style={{ width: '100%' }} />
                                <br/>

                        </Paper>
                        </Link>
                        </Grid>
                        
                        <Grid item xs ={12} lg ={6}>
                        <Link to ='/listmenu' style={{textDecoration: 'none' }}>                              
                        <br/>
                        <Typography variant="body1" component="p" color="textPrimary" style={{fontWeight: 'lighter', color: '#828282'}}>Drum</Typography>
                        <Typography variant="h5" component="p" color="textPrimary" style={{fontWeight: 'bold', color: '#333', marginBottom : '20px'}} >Kursus Drummer Special Coach (Eno Netral)</Typography>
                        <Typography variant="h7" component="p" color="textPrimary" style={{fontWeight: 'bold', color: '#5D5FEF', marginBottom : '20px' }}>IDR 8.500.000</Typography>
                        </Link>
                        <Grid item xs ={12} lg = {12} style={{marginBottom : '20px',marginTop : '20px' }}>
                        <DetailKelasComboBox/>
                        </Grid>
                        <Grid item xs ={12} lg = {12}>
                        <Stack spacing={3} direction="row">
                                <Link to = '/successpurchase' style={{textDecoration: 'none' }}>
                             <Button  variant="outlined" fullWidth   style={{fontWeight: 'bold', marginTop: '50px', backgroundColor: 'white', color: '#5D5FEF', outlineColor : '5D5FEF' }}>Masukkan Keranjang</Button>  
                               </Link> 
                                <Link to = '/successpurchase' style={{textDecoration: 'none' }}>
                             <Button variant="contained" fullWidth style= {{fontWeight: 'bold', marginTop: '50px', backgroundColor: '#5D5FEF', color: 'white', }}>Beli Sekarang</Button>
                             </Link>
                        </Stack>
                        </Grid>
                        </Grid>
                </Grid>



                <Box sx ={{backgroundColor:'#fff', boxShadow: 'none', display: 'flex', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'start', borderRadius: 0}} >
                        <Grid container spacing={6}>
                                <Grid item lg={12} xs={12}>
                                <Typography variant="h5" component="h4" color="secondary" style={{ fontWeight: 'bold',color : '#000000', marginTop : '60px', marginBottom : '30px',marginLeft: '10px'}}>
                                Deskripsi
                                </Typography>
                                <Typography variant="body1" component="h4" color="textPrimary" style={{ fontSize: '16px',marginTop :'0px', marginBottom : '20px',marginLeft: '10px' }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </Typography>
                                <Typography variant="body1" component="h4" color="textPrimary" style={{ fontSize: '16px',marginTop :'0px', marginBottom : '30px',marginLeft: '10px' }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </Typography>
                                </Grid>
                        </Grid>
                </Box>
                </Container>
        
                </>
  )
}
export default DetailKelas1