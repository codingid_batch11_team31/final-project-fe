import React from 'react';
import { Box, Button, Container, Grid, Paper, Stack, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import { useDemoData } from '@mui/x-data-grid-generator';
import { DataGrid } from '@mui/x-data-grid';

export default function Invoice1 (){
        return (
            <>
            <Container >
                    <Grid Container spacing ={4} sx = {{marginTop : '100px'}}>
                            <Grid item lg ={12} xs= {12}>
                                <Stack direction='row'>
                                <Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginBottom: '20px',  }}>
                                Beranda
                                <span style={{ marginLeft: '10px' }}>&gt;</span>
                                </Typography>
                                       
                                <Typography Typography variant="body2" style={{ marginLeft: '10px',display: 'flex', alignItems: 'center', fontWeight: 'light', color: '#5D5FEF', marginBottom: '20px' }}>
                                          Invoice
                                </Typography>
                                </Stack>
                            </Grid>
                    </Grid>


                    <Grid Container spacing ={4} sx = {{marginTop : '10px'}}>
                            <Grid item lg ={12} xs= {4}>
                            <Typography Typography variant="body1" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '20px' }}>
                                         Invoice
                                </Typography>

                            </Grid>
                    </Grid>

                    
                    <Grid Container spacing ={4} sx = {{ marginBottom : '10px', display: 'flex', flexDirection: 'column'}}>
                            
                            <Stack  spacing = {10} direction = 'row'>
                                <Grid item lg ={12} xs= {12} >
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                         No
                                </Typography>
                                </Grid>

                                <Grid item lg ={12} xs= {12} >
                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                         No. Invoice
                                </Typography>
                                </Grid>

                                 <Grid item lg ={12} xs= {12} >
                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                         Tanggal Beli

                                </Typography>
                                </Grid>

                                <Grid item lg ={12} xs= {12} >
                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px', }}>
                                        Jumlah Kursus
                                </Typography>
                                </Grid>

                                <Grid item lg ={12} xs= {12} >
                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px' ,marginTop: '10px',}}>
                                         Total Harga
                                </Typography>
                                </Grid> 

                                <Grid item lg ={12} xs= {12} >
                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'bold', color: 'black', marginBottom: '10px',marginTop: '10px' }}>
                                         Action
                                </Typography>
                                </Grid>
                                
                            </Stack>

                        </Grid>
                                

                                
                         <Grid Container spacing ={4} sx = {{ marginBottom : '10px', display: 'flex', flexDirection: 'column'}}>
                           <Grid item lg ={12} xs= {3} sx ={{backgroundColor: '#white', padding: '8px'}}>
                            <Stack  spacing = {10} direction = 'row' >
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', }}>
                                         1
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '100px'}}>
                                         APM00003
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black',marginRight: '0px',marginLeft: '80px'  }}>
                                         12 Juni 2022

                                </Typography>

                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '90px' }}>
                                         2
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '150px' }}>
                                         IDR 11.500.000
                                </Typography>
                                <Link to = '/detailinvoice' sx = {{textDecoration : 'none'}}>
                                <Button variant="contained"  sx={{ width: '100%' }}  style={{display: 'flex',fontWeight: 'bold',backgroundColor: "#5D5FEF", color: "white", marginRight: '50px',marginLeft: '50px'}} > Rincian </Button>
                                </Link>
                            </Stack>

                      </Grid> 
                {/* ===== =============================================================================================================================================================================================*/}
                        {/* <Grid Container spacing ={4} sx = {{marginTop : '10px', marginBottom : '10px', display: 'flex', flexDirection: 'column'}}>
                        <Grid item lg ={12} xs= {3} sx ={{backgroundColor: '#F2C94C33', padding: '8px'}}>
                            <Stack  spacing = {10} direction = 'row' >
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', }}>
                                         2
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '100px'}}>
                                         APM00002
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black',marginRight: '0px',marginLeft: '80px'  }}>
                                         12 Juni 2022

                                </Typography>

                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '90px' }}>
                                         1
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '150px' }}>
                                         IDR 11.500.000
                                </Typography>
                                 <Link to = '/detailinvoice' sx = {{textDecoration : 'none'}}>
                                <Button variant="contained"  sx={{ width: '100%'}}  style={{display: 'flex',fontWeight: 'bold',backgroundColor: "#5D5FEF", color: "white", marginRight: '50px',marginLeft: '50px'}} > Rincian </Button>
                                 </Link>
                            </Stack>
                            
                        </Grid>
                        </Grid> */}
 {/* ===== ============================================================================================================================================================================================= */}
                        <Grid Container spacing ={4} sx = {{ marginBottom : '20px', display: 'flex', flexDirection: 'column',marginBottom : '250px'}}>
                        <Grid item lg ={12} xs= {3} sx ={{backgroundColor: '#white', padding: '8px'}}>
                            <Stack  spacing = {10} direction = 'row' >
                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', }}>
                                         3
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '100px'}}>
                                         APM00001
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black',marginRight: '0px',marginLeft: '80px'  }}>
                                         12 Juni 2022

                                </Typography>

                                <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '90px' }}>
                                         1
                                </Typography>

                                 <Typography Typography variant="body2" style={{ display: 'flex', alignItems: 'center', fontWeight: 'light', color: 'black', marginRight: '0px',marginLeft: '150px' }}>
                                         IDR 11.500.000
                                </Typography>
                                 <Link to = '/detailinvoice' sx = {{textDecoration : 'none'}}>
                                <Button variant="contained" sx={{ width: '100%' }} style={{display: 'flex',fontWeight: 'bold',backgroundColor: "#5D5FEF", color: "white", marginRight: '50px',marginLeft: '50px'}} > Rincian </Button>
                                 </Link>
                            </Stack>
                        </Grid>
                      </Grid> 
                    </Grid>
                     {/* ===== =============================================================================================================================================================================================*/}
               
            </Container>
            </>
        )
}