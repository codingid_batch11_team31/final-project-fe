// import React from 'react';
// import { Container } from "@mui/system";
// import { Grid, Typography } from "@mui/material";
// import { Link } from "react-router-dom";
// import HeadDrawer from "../components/HeadDrawer";
// import useRegister from "../hooks/useRegister";
// import RegisterForm from "../components/RegisterForm";

// const RegisterPage = () => {
//   const {
//     registerButtonFunc,
//     setUserName,
//     setEmail,
//     setPassword,
//     setConfirmPassword,
//     confirmPassword,
//     errorMessage,
//     email,
//     userName,
//     password,
//   } = useRegister();

//   const handleRegister = () => {
//     // Perform register validation here
//     // Call your register function here
//     registerButtonFunc();
//   };

//   return (
//     <div>
//       <HeadDrawer/>
//       <Container style={{ display: "flex", flexDirection: "column", gap: "12px", marginBottom : "100px" }}>
//         <Grid container spacing={2}>
//           <Grid item lg={3}></Grid>
//           <Grid item lg={6} xs={12}>
//             <RegisterForm
//               userName={userName}
//               email={email}
//               password={password}
//               confirmPassword={confirmPassword}
//               onChange={(field) => (e) => {
//                 switch (field) {
//                   case 'userName':
//                     setUserName(e.target.value);
//                     break;
//                   case 'email':
//                     setEmail(e.target.value);
//                     break;
//                   case 'password':
//                     setPassword(e.target.value);
//                     break;
//                   case 'confirmPassword':
//                     setConfirmPassword(e.target.value);
//                     break;
//                   default:
//                     break;
//                 }
//               }}
//               onSubmit={handleRegister}
//             />
//             <Grid container spacing={0}>
//               <Grid item lg={4} xs={6}>
//                 <Typography variant="body1" component="p" color="textPrimary" style={{marginTop: "10px"}}>
//                   Sudah punya akun?
//                 </Typography>
//               </Grid>
//               <Grid item lg={2} xs={6}>
//                 <Typography variant="body1" component="p" color="primary" style={{marginTop: "10px"}}>
//                   <Link to='/login'>Login disini</Link>
//                 </Typography>
//               </Grid>
//             </Grid>
//           </Grid>
//         </Grid>
//       </Container>
//     </div>
//   );
// }

// export default RegisterPage;

// RegisterPage.js

import React from 'react';
import { Container } from "@mui/system";
import { Grid, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import HeadDrawer from "../components/HeadDrawer";
import useRegister from "../hooks/useRegister";
import RegisterForm from "../components/RegisterForm";

const RegisterPage = () => {
  const {
    registerButtonFunc,
    setUserName,
    setEmail,
    setPassword,
    setConfirmPassword,
    confirmPassword,
    isEmailRegistered,
    errorMessage,
    email,
    userName,
    password,
  } = useRegister();

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      await registerButtonFunc();

      // Tampilkan pesan jika email terdaftar
      if (isEmailRegistered) {
        alert("Email sudah terdaftar. Gunakan email lain.");
      }
    } catch (error) {
      console.error("Error during registration:", error.response?.data);
      // Tampilkan pesan kesalahan dari useRegister
      alert("Terjadi kesalahan saat registrasi.");
    }
  };


  return (
    <div>
      <HeadDrawer/>
      <Container style={{ display: "flex", flexDirection: "column", gap: "12px", marginBottom : "100px" }}>
        <Grid container spacing={2}>
          <Grid item lg={3}></Grid>
          <Grid item lg={6} xs={12}>
            <RegisterForm
              userName={userName}
              email={email}
              password={password}
              confirmPassword={confirmPassword}
              onChange={(field) => (e) => {
                switch (field) {
                  case 'userName':
                    setUserName(e.target.value);
                    break;
                  case 'email':
                    setEmail(e.target.value);
                    break;
                  case 'password':
                    setPassword(e.target.value);
                    break;
                  case 'confirmPassword':
                    setConfirmPassword(e.target.value);
                    break;
                  default:
                    break;
                }
              }}
              onSubmit={handleRegister}
            />

            {/* Tambahkan pesan kesalahan di bawah form */}
            {isEmailRegistered && (
              <Typography variant="body1" component="p" color="error" style={{ marginTop: "10px"}}>
                Email sudah terdaftar. Gunakan email lain.
              </Typography>
            )}

            {/* Tampilkan pesan kesalahan dari useRegister */}
            {errorMessage && (
              <Typography variant="body1" component="p" color="error" style={{ marginTop: "10px"}}>
                {errorMessage}
              </Typography>
            )}

            <Grid container spacing={0}>
              <Grid item lg={4} xs={6}>
                <Typography variant="body1" component="p" color="textPrimary" style={{marginTop: "10px"}}>
                  Sudah punya akun?
                </Typography>
              </Grid>
              <Grid item lg={2} xs={6}>
                <Typography variant="body1" component="p" color="primary" style={{marginTop: "10px"}}>
                  <Link to='/login'>Login disini</Link>
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default RegisterPage;
