import axios from "axios";
import { useState } from "react";

const useRegister = () => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const registerButtonFunc = () => {
    console.log(password === confirmPassword);
    if (password === confirmPassword) {
      axios
        .post(
          `${import.meta.env.VITE_API_URL}api/Auth/Register`,
          {
            nama: userName,
            password: password,
            email: email,
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then(() => {
          alert("silahkan cek email!!");
        })
        .catch((err) => {
          setErrorMessage(err.response.data);
        });
    } else {
      setErrorMessage("Password dan Confirm password tidak sama");
    }
  };

  return {
    registerButtonFunc,
    setUserName,
    setEmail,
    setPassword,
    setConfirmPassword,
    password,
    confirmPassword,
    setErrorMessage,
    errorMessage,
  };
};

export default useRegister;
